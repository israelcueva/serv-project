package com.demo.config;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.demo.controller.CategoriaController;
import com.demo.controller.ClienteController;
import com.demo.controller.FormaPagoController;
import com.demo.controller.ProductoController;

@ApplicationPath("rest")
public class DemoApplication extends Application {
	
	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> classes = new HashSet<>();
		classes.add(ClienteController.class);
		classes.add(CategoriaController.class);
		classes.add(ProductoController.class);
		classes.add(FormaPagoController.class);		
		return classes;
	}
}
