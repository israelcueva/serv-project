package com.demo.controller;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import com.demo.model.Categoria;
import com.demo.service.ICategoriaService;

@Path("/categorias")
public class CategoriaController {
	
	@Inject
	private ICategoriaService service;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Categoria> listar() {
		try {
			return service.listar();
		} catch (Exception e) {
			throw new WebApplicationException(403);
		}
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Categoria buscar(@PathParam("id") int id) {
		try {
			Categoria categoria = service.buscarID(id);
			if(categoria == null)
				throw new WebApplicationException(404);
			return categoria;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new WebApplicationException(500);
		}
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Categoria save(@Valid Categoria categoria) {
		System.out.println(categoria.toString());
		try {
			return service.registrar(categoria);
		} catch (Exception e) {
			throw new WebApplicationException(500);
		}
	}
	
	@PUT
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Categoria update(Categoria categoria) {
		try {
			return service.modficar(categoria);
		} catch (Exception e) {
			throw new WebApplicationException(404);
		}
	}
	
}
