package com.demo.controller;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import com.demo.model.Cliente;
import com.demo.service.IClienteService;

@Path("/clientes")
public class ClienteController {

	@Inject
	private IClienteService service;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Cliente> listar() {
		try {
			return service.listar();
		} catch (Exception e) {
			throw new WebApplicationException(403);
		}
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Cliente buscar(@PathParam("id") int id) {
		try {
			Cliente cliente = service.buscarID(id);
			if(cliente == null)
				throw new WebApplicationException(404);
			return cliente;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new WebApplicationException(500);
		}
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Cliente save(@Valid Cliente cliente) {
		System.out.println(cliente.toString());
		try {
			return service.registrar(cliente);
		} catch (Exception e) {
			throw new WebApplicationException(500);
		}
	}
	
	@PUT
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Cliente update(Cliente cliente) {
		try {
			return service.modficar(cliente);
		} catch (Exception e) {
			throw new WebApplicationException(404);
		}
	}
}
