package com.demo.controller;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import com.demo.model.FormaPago;
import com.demo.service.IFormaPagoService;

@Path("/formapagos")
public class FormaPagoController {
	
	@Inject
	private IFormaPagoService service;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<FormaPago> listar() {
		try {
			return service.listar();
		} catch (Exception e) {
			throw new WebApplicationException(403);
		}
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public FormaPago buscar(@PathParam("id") int id) {
		try {
			FormaPago formaPago = service.buscarID(id);
			if(formaPago == null)
				throw new WebApplicationException(404);
			return formaPago;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new WebApplicationException(500);
		}
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public FormaPago save(@Valid FormaPago formaPago) {
		System.out.println(formaPago.toString());
		try {
			return service.registrar(formaPago);
		} catch (Exception e) {
			throw new WebApplicationException(500);
		}
	}
	
	@PUT
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public FormaPago update(FormaPago formaPago) {
		try {
			return service.modficar(formaPago);
		} catch (Exception e) {
			throw new WebApplicationException(404);
		}
	}

}
