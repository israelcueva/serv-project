package com.demo.controller;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import com.demo.model.Producto;
import com.demo.service.IProductoService;

@Path("/productos")
public class ProductoController {
	
	@Inject
	private IProductoService service;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Producto> listar() {
		try {
			return service.listar();
		} catch (Exception e) {
			throw new WebApplicationException(403);
		}
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Producto buscar(@PathParam("id") int id) {
		try {
			Producto producto = service.buscarID(id);
			if(producto == null)
				throw new WebApplicationException(404);
			return producto;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new WebApplicationException(500);
		}
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Producto save(@Valid Producto producto) {
		System.out.println(producto.toString());
		try {
			return service.registrar(producto);
		} catch (Exception e) {
			throw new WebApplicationException(500);
		}
	}
	
	@PUT
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Producto update(Producto producto) {
		try {
			return service.modficar(producto);
		} catch (Exception e) {
			throw new WebApplicationException(404);
		}
	}

}
