package com.demo.dao;

import java.util.List;

public interface ICRUD<T> {	
	
	T registrar(T t);
	
	T modficar(T t);
	
	T buscarID(int id);
	
	List<T> listar();
	
}