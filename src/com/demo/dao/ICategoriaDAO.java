package com.demo.dao;

import javax.ejb.Local;

import com.demo.model.Categoria;

@Local
public interface ICategoriaDAO extends ICRUD<Categoria> {

}
