package com.demo.dao;

import javax.ejb.Local;

import com.demo.model.Cliente;

@Local
public interface IClienteDAO extends ICRUD<Cliente> {

}
