package com.demo.dao;

import javax.ejb.Local;

import com.demo.model.FormaPago;

@Local
public interface IFormaPagoDAO extends ICRUD<FormaPago> {

}
