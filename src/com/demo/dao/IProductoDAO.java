package com.demo.dao;

import javax.ejb.Local;

import com.demo.model.Producto;

@Local
public interface IProductoDAO extends ICRUD<Producto> {

}
