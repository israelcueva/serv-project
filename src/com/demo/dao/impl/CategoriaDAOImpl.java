package com.demo.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.demo.dao.ICategoriaDAO;
import com.demo.model.Categoria;

//http://tomee.apache.org/openjpa.html
@Stateless
public class CategoriaDAOImpl implements ICategoriaDAO {
	
	@PersistenceContext(unitName="BD_EC2_WS_PU")
	private EntityManager em; 

	@Override
	public Categoria registrar(Categoria t) {
		em.persist(t);
		em.flush();
		return t;
	}

	@Override
	public Categoria modficar(Categoria t) {
		em.merge(t);
		em.flush();
		return t;
	}

	@Override
	public Categoria buscarID(int id) {
		List<Categoria> lista = new ArrayList<>();
		Query q = em.createQuery("FROM Categoria c where c.idCategoria = ?1 ");
		q.setParameter(1, id);
		lista = (List<Categoria>) q.getResultList();
		
		Categoria cat = lista != null && !lista.isEmpty() ? lista.get(0) : new Categoria();
		return cat;
	}

	@Override
	public List<Categoria> listar() {
		List<Categoria> lista = new ArrayList<>();
		Query q = em.createQuery("FROM Categoria c");
		lista = (List<Categoria>) q.getResultList();
		return lista;
	}

}
