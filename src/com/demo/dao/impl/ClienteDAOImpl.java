package com.demo.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.demo.dao.IClienteDAO;
import com.demo.model.Cliente;

//http://tomee.apache.org/openjpa.html
@Stateless
public class ClienteDAOImpl implements IClienteDAO{
	
	@PersistenceContext(unitName="BD_EC2_WS_PU")
	private EntityManager em; 

	@Override
	public Cliente registrar(Cliente t) {
		em.persist(t);
		em.flush();
		return t;
	}

	@Override
	public Cliente modficar(Cliente t) {
		em.merge(t);
		em.flush();
		return t;
	}

	@Override
	public Cliente buscarID(int id) {
		List<Cliente> lista = new ArrayList<>();
		Query q = em.createQuery("FROM Cliente c where c.idCliente = ?1 ");
		q.setParameter(1, id);
		lista = (List<Cliente>) q.getResultList();
		
		Cliente cli = lista != null && !lista.isEmpty() ? lista.get(0) : new Cliente();
		return cli;
	}

	@Override
	public List<Cliente> listar() {
		List<Cliente> lista = new ArrayList<>();
		Query q = em.createQuery("FROM Cliente c");
		lista = (List<Cliente>) q.getResultList();
		return lista;
	}

}
