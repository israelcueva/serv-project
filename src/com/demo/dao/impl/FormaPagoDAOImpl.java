package com.demo.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.demo.dao.IFormaPagoDAO;
import com.demo.model.FormaPago;

@Stateless
public class FormaPagoDAOImpl implements IFormaPagoDAO {
	
	@PersistenceContext(unitName="BD_EC2_WS_PU")
	private EntityManager em;

	@Override
	public FormaPago registrar(FormaPago t) {
		em.persist(t);
		em.flush();
		return t;
	}

	@Override
	public FormaPago modficar(FormaPago t) {
		em.merge(t);
		em.flush();
		return t;
	}

	@Override
	public FormaPago buscarID(int id) {
		List<FormaPago> lista = new ArrayList<>();
		Query q = em.createQuery("FROM FormaPago p where p.idFormaPago = ?1 ");
		q.setParameter(1, id);
		lista = (List<FormaPago>) q.getResultList();
		
		FormaPago fp = lista != null && !lista.isEmpty() ? lista.get(0) : new FormaPago();
		return fp;
	}

	@Override
	public List<FormaPago> listar() {
		List<FormaPago> lista = new ArrayList<>();
		Query q = em.createQuery("FROM FormaPago p");
		lista = (List<FormaPago>) q.getResultList();
		return lista;
	} 

}
