package com.demo.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.demo.dao.IProductoDAO;
import com.demo.model.Producto;

@Stateless
public class ProductoDAOImpl implements IProductoDAO {

	@PersistenceContext(unitName="BD_EC2_WS_PU")
	private EntityManager em; 
	
	@Override
	public Producto registrar(Producto t) {
		em.persist(t);
		em.flush();
		return t;
	}

	@Override
	public Producto modficar(Producto t) {
		em.merge(t);
		em.flush();
		return t;
	}

	@Override
	public Producto buscarID(int id) {
		List<Producto> lista = new ArrayList<>();
		Query q = em.createQuery("FROM Producto p where p.idProducto = ?1 ");
		q.setParameter(1, id);
		lista = (List<Producto>) q.getResultList();
		
		Producto pto = lista != null && !lista.isEmpty() ? lista.get(0) : new Producto();
		return pto;
	}

	@Override
	public List<Producto> listar() {
		List<Producto> lista = new ArrayList<>();
		Query q = em.createQuery("FROM Producto p");
		lista = (List<Producto>) q.getResultList();
		return lista;
	}

}
