package com.demo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="cliente")
public class Cliente implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CLI_ID")
	private Integer idCliente;
	@Column(name = "CLI_Apellidos",  length = 100, nullable = false )
	private String apellidos;
	@Column(name = "CLI_Nombres", length = 100, nullable = false)
	private String nombres;
	@Column(name = "CLI_DNI", length = 20, nullable = false)
	private String dni;
	@Column(name = "CLI_Telefono", length = 10, nullable = false)
	private String telefono;
	@Column(name = "CLI_Email", length = 30, nullable = false)
	private String email;
	@Column(name = "CLI_Direccion", length = 500, nullable = false)
	private String direccion;
	@Column(name = "CLI_Estado", nullable = false)
	private Boolean estado;
	
	@Transient
	private String nombreCompleto;

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public String getNombreCompleto() {
		StringBuilder sb = new StringBuilder();
		sb.append(apellidos).append(",").append(nombres);
		return sb.toString();
	}

	@Override
	public String toString() {
		return "Persona [ID Cliente= " + idCliente + ", Apellidos= " + apellidos + ", Nombres= " + nombres + ", DNI= "
				+ dni + ", Tel�fono=" + telefono + ", Email=" + email  +", Direcci�n= "+ direccion + ", Estado= "
				+ estado + ", nombreCompleto=" + nombreCompleto + "]";
	}
	
	

}
