package com.demo.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="producto")
public class Producto implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PTO_ID")
	private Integer idProducto;
	@Column(name = "PTO_Nombre", length = 30, nullable = false)
	private String nombre;
	@Column(name = "PTO_Detalle", length = 500, nullable = false)
	private String detalle;
	@Column(name = "PTO_Precio", precision = 4, scale = 2, nullable = false)
	private Float precio;
	@Column(name = "PTO_Peso", precision = 4, scale = 2, nullable = false)
	private Float peso;
	@Column(name = "PTO_Cantidad", nullable = false)
	private Integer cantidad;
	@Column(name = "PTO_Ruta", length = 500, nullable = false)
	private String ruta;
	@Column(name = "PTO_Estado", nullable = false)
	private Boolean estado;
	
	@JoinColumn(name = "CAT_ID", nullable = false)
	@ManyToOne  //(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Categoria categoria;

	public Integer getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public Float getPrecio() {
		return precio;
	}

	public void setPrecio(Float precio) {
		this.precio = precio;
	}

	public Float getPeso() {
		return peso;
	}

	public void setPeso(Float peso) {
		this.peso = peso;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}	
}
