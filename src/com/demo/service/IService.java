package com.demo.service;

import java.util.List;

public interface IService<T> {
	
	T registrar(T t);

	T modficar(T t);

	T buscarID(int id);

	List<T> listar();

}
