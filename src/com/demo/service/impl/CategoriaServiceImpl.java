package com.demo.service.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import com.demo.dao.ICategoriaDAO;
import com.demo.model.Categoria;
import com.demo.service.ICategoriaService;

@Named
public class CategoriaServiceImpl implements ICategoriaService {
	
	@EJB
	private ICategoriaDAO dao;

	@Override
	public Categoria registrar(Categoria t) {
		return dao.registrar(t);
	}

	@Override
	public Categoria modficar(Categoria t) {
		return dao.modficar(t);
	}

	@Override
	public Categoria buscarID(int id) {
		return dao.buscarID(id);
	}

	@Override
	public List<Categoria> listar() {
		return dao.listar();
	}

}
