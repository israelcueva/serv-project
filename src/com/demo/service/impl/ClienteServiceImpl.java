package com.demo.service.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import com.demo.dao.IClienteDAO;
import com.demo.model.Cliente;
import com.demo.service.IClienteService;

@Named
public class ClienteServiceImpl implements IClienteService{
	
	@EJB
	private IClienteDAO dao;

	@Override
	public Cliente registrar(Cliente t) {
		return dao.registrar(t);
	}

	@Override
	public Cliente modficar(Cliente t) {
		return dao.modficar(t);
	}

	@Override
	public Cliente buscarID(int id) {
		return dao.buscarID(id);
	}

	@Override
	public List<Cliente> listar() {
		return dao.listar();
	}

}
