package com.demo.service.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import com.demo.dao.IFormaPagoDAO;
import com.demo.model.FormaPago;
import com.demo.service.IFormaPagoService;

@Named
public class FormaPagoServiceImpl implements IFormaPagoService{
	
	@EJB
	private IFormaPagoDAO dao;

	@Override
	public FormaPago registrar(FormaPago t) {
		return dao.registrar(t);
	}

	@Override
	public FormaPago modficar(FormaPago t) {
		return dao.modficar(t);
	}

	@Override
	public FormaPago buscarID(int id) {
		return dao.buscarID(id);
	}

	@Override
	public List<FormaPago> listar() {
		return dao.listar();
	}

}
