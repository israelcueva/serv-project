package com.demo.service.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import com.demo.dao.IProductoDAO;
import com.demo.model.Producto;
import com.demo.service.IProductoService;

@Named
public class ProductoServiceImpl implements IProductoService {

	@EJB
	private IProductoDAO dao;

	@Override
	public Producto registrar(Producto t) {
		return dao.registrar(t);
	}

	@Override
	public Producto modficar(Producto t) {
		return dao.modficar(t);
	}

	@Override
	public Producto buscarID(int id) {
		return dao.buscarID(id);
	}

	@Override
	public List<Producto> listar() {
		return dao.listar();
	}
	


}
